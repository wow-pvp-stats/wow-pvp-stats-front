import Vue from "vue";
import axios from "axios";
import VueLodash from "vue-lodash";
import lodash from "lodash";
import VueRamda from "vue-ramda";
import router from "./router";
import { BootstrapVue } from "bootstrap-vue";

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import "@fortawesome/fontawesome-free/css/all.css";
import "@fortawesome/fontawesome-free/js/all.js";

import App from "./App.vue";

Vue.config.productionTip = false;
Vue.prototype.$http = axios;

Vue.use(VueRamda);
Vue.use(VueLodash, { lodash: lodash });
Vue.use(BootstrapVue);

new Vue({
  render: h => h(App),
  router
}).$mount("#app");
